'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');


var Publication = require('../models/publication');
var User = require('../models/user');
var Follow = require('../models/follow');


// Crear publicacion

function savePublication(req,res){
    var params = req.body;

    if(!params.text) return res.status(200).send({message:'Rellena los campos'});

    var publication = new Publication();

    publication.text = params.text;
    publication.file = 'null';
    publication.user = req.user.sub;
    publication.created_at = moment().unix();

    publication.save((err,publicationStored)=>{
        if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar la publicación'});

        if(!publicationStored) return res.status(404).send({message:'Ha ocurrido un error al guardar la publicación'});

        return res.status(200).send({
            publication: publicationStored
        });
    });

}

// Publicaciones de los usuarios a los que sigo

function getPublications(req,res){
    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 4;

    Follow.find({user: req.user.sub}).populate("followed").exec((err,follows)=>{
        if(err) return res.status(500).send({message:'Error al devolver el seguimiento'});

        var follows_clean = [];

        // Usuarios con publicaciones que seguimos

        follows.forEach((follow)=>{
           follows_clean.push(follow.followed);
        });
        // Para ver nuestras publicaciones
        follows_clean.push(req.user.sub);

        // $in busca dentro de un array las coincidencias

        Publication.find({user: {$in: follows_clean }})
            .sort('-created_at')
            .populate('user')
            .paginate(page,itemsPerPage,(err,publications,total)=>{
                if(err) return res.status(500).send({message:'Error al devolver publicaciones'});

                if(!publications) return res.status(404).send({message:'No hay publicaciones'});

                return res.status(200).send({
                    page,
                    itemsPerPage,
                    total_items:total,
                    total_pages: Math.ceil(total/itemsPerPage),
                    publications: publications,
                });
            });


    });
}

// Publicaciones del usuario
function getPublicationsUser(req,res){
    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var user = req.user.sub;
    if(req.params.user){
        user = req.params.user;
    }

    var itemsPerPage = 4;
            Publication.find({user: user})
            .sort('-created_at')
            .populate('user')
            .paginate(page,itemsPerPage,(err,publications,total)=>{
                if(err) return res.status(500).send({message:'Error al devolver publicaciones'});

                if(!publications) return res.status(404).send({message:'No hay publicaciones'});

                return res.status(200).send({
                    page,
                    itemsPerPage,
                    total_items:total,
                    total_pages: Math.ceil(total/itemsPerPage),
                    publications: publications,
                });
            });

}

// Ver una publicacion

function getPublication(req,res){
    var publicationId = req.params.id;

    Publication.findById(publicationId, (err,publication)=>{
        if(err) return res.status(500).send({message:'Error al devolver la publicacion'});

        if(!publication) return res.status(404).send({message:'No existe esa publicacion'});

        return res.status(200).send({
           publication:publication,
        });
    });
}

// Eliminar publicacion

function deletePublicacion(req,res){
    var publicationId = req.params.id;


    Publication.find({user:req.user.sub, '_id':publicationId }).deleteOne((err,publicationRemoved)=>{

        if(err) return res.status(500).send({message:'Error al borrar la publicacion'});

        if(!publicationRemoved) return res.status(404).send({message:'No existe esa publicacion'});


        return res.status(200).send({
            message:'Publicación eliminada',
        });

    });
}




// Subir archivos de imagen
function uploadImage(req,res){
    var publicationId = req.params.id;

    if(req.files){
        var file_path = req.files.image.path;

        var file_split = file_path.split('\\');

        var file_name = file_split[2];

        var ext_split = file_name.split('\.');

        var file_ext = ext_split[1];

        if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif'){


            // Permisos para modificar

            Publication.findOne({'user':req.user.sub, '_id':publicationId}).exec((err,publication)=>{
               if(publication){
                   // Actualizar bd
                   Publication.findByIdAndUpdate(publicationId, {file: file_name}, {new:true}, (err,publicationUpdated) => {
                       if(err) return res.status(500).send({message: 'Error en la petición'});

                       if(!publicationUpdated) return res.status(404).send({message: 'No se ha podido actualizar'});

                       return res.status(200).send({
                           publication: publicationUpdated,
                       });
                   });
               } else{
                   removeFileUploads(res, file_path, 'No tienes permiso para actualizar esta publicación ');
               }
            });


        }else{
            removeFileUploads(res, file_path, 'Extension no valida');
        }
    }else{
        return res.status(200).send({message:'No se han subido imagenes'});
    }

}

function removeFileUploads(res, file_path, message){
    fs.unlink(file_path, (err) => {
        return res.status(200).send({message:message});
    });
}


function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/publications/'+image_file;

    fs.exists(path_file, (exists) => {
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message: 'No existe la imagen'});
        }
    })
}





module.exports = {
    savePublication,
    getPublications,
    deletePublicacion,
    getPublication,
    getPublicationsUser,
    uploadImage,
    getImageFile,
};