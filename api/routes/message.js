'use strict'

var express = require('express');
var MessageController = require('../controllers/message');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();

api.get('/my-messages/:page?',md_auth.ensureAuth,MessageController.getReceivedMessages);
api.get('/my-sent-messages/:page?',md_auth.ensureAuth,MessageController.getEmitMessages);
api.get('/unviewed-messages',md_auth.ensureAuth,MessageController.getUnviewedMessages);
api.get('/set-viewed-messages',md_auth.ensureAuth,MessageController.setViewedMessages);

api.post('/message',md_auth.ensureAuth,MessageController.saveMessage);


module.exports = api;