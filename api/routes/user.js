'use strict'

var express = require('express');
var UserController = require('../controllers/user');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();

// Subida imagen

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: 'uploads/users'});



api.get('/home',md_auth.ensureAuth, UserController.home);

api.get('/user/:id',md_auth.ensureAuth, UserController.getUser);
api.get('/users/:page?', md_auth.ensureAuth, UserController.getUsers);
api.get('/get-image-user/:imageFile', UserController.getImageFile);
api.get('/counters/:id?',md_auth.ensureAuth, UserController.getCounters);

api.post('/register', UserController.saveUser);
api.post('/login', UserController.loginUser);
api.post('/upload-image-user/:id',[md_auth.ensureAuth, md_upload], UserController.uploadImage);



api.put('/update-user/:id', md_auth.ensureAuth, UserController.updateUser);


module.exports = api;