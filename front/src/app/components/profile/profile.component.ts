import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../../models/user";
import {Follow} from "../../models/follow";
import {UserService} from "../../services/user.service";
import {FollowService} from "../../services/follow.service";
import {GLOBAL} from "../../services/global";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
    providers: [UserService,FollowService],
})
export class ProfileComponent implements OnInit {
  public title:string;
  public user: User;
  public status: string;
  public identity;
  public url;
  public token;
  public stats;
  public following;
  public followed;


  constructor(private _route:ActivatedRoute,
              private _router:Router,
              private _userService:UserService,
              private _followService:FollowService)
              {
                this.title = 'Perfil';
                this.token = this._userService.getToken();
                this.identity = this._userService.getIdentity();
                this.stats = this._userService.getStats();
                this.url = GLOBAL.url;
                this.followed = false;
                this.following = false;
              }

  ngOnInit() {
    this.loadPage();
  }

  loadPage(){
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.getCounters(id);
      this.getUser(id);

    });

  }

  getUser(id){
    this._userService.getUser(id).subscribe(
        response => {
          if(response.user){
            this.user = response.user;

            if(response.following && response.following.user){
              this.following = true;
            }else{
              this.following = false;
            }

            if(response.followed && response.followed.user){
              this.followed = true;
            }else{
                this.followed = false;
            }


          }else{
            this.status = 'error';
          }
        },
        error => {
          // Si no existe usuario te redirige al tuyo
          this._router.navigate(['/perfil',this.identity._id]);
        }
    )
  }

  getCounters(id){
    this._userService.getCounters(id).subscribe(
        response => {
          this.stats = response;
        },
        error => {
            var errorMessage = <any>error;
            console.log(errorMessage);
        }
    )

  }

  followUser(followed){
    var follow = new Follow('',this.identity._id,followed);
    this._followService.addFollow(this.token, follow).subscribe(
        response => {
          this.following = true;
        },
        error => {
            var errorMessage = <any>error;
            console.log(errorMessage);
        }
    )
  }

    unfollowUser(followed){
        this._followService.deleteFollow(this.token, followed).subscribe(
            response => {
                this.following = false;
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
            }
        )
    }

    public followUserOver;
    mouseEnter(user_id){
      this.followUserOver = user_id;
    }

    mouseLeave(){
        this.followUserOver = null;
    }

}
