import { Component, OnInit, Input } from '@angular/core';
import { Router,ActivatedRoute, Params} from "@angular/router";
import { Publication } from "../../models/publication";
import { UserService } from "../../services/user.service";
import { PublicationService } from "../../services/publication.service";
import { GLOBAL } from "../../services/global";
import * as $ from 'jquery';


@Component({
    selector: 'app-timeline',
    templateUrl: './timeline.component.html',
    styleUrls: ['./timeline.component.scss'],
    providers: [UserService, PublicationService],
})
export class TimelineComponent implements OnInit {

    public identity;
    public token;
    public title:string;
    public url:string;
    public itemsPerPage;
    public page;
    public total;
    public pages;
    public publications: Publication[];
    public status:string;
    public showImage;
    @Input() user: string;


    constructor(private _userService:UserService,
                private _route: ActivatedRoute,
                private _router: Router,
                private _publicationService:PublicationService)
    {
        this.title = 'Publicaciones';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.url = GLOBAL.url;
        this.page = 1;
    }

    ngOnInit() {
        this.getPublications(this.user,this.page);
    }


    getPublications(user, page, adding = false){
        this._publicationService.getPublicationsUser(this.token,user,page).subscribe(
            response => {
                if(response.publications){
                    this.total = response.total_items;
                    this.pages = response.total_pages;
                    this.itemsPerPage = response.itemsPerPage;
                    if(!adding){
                        this.publications = response.publications;
                    }else{
                        var arrayA = this.publications;
                        // Hacemos 2 arrays para mergearlo con los nuevos
                        var arrayB = response.publications;

                        this.publications = arrayA.concat(arrayB);

                        $("html, body").animate({ scrollTop: $('body').prop('scrollHeight')},500);

                    }

                }else{
                    this.status = 'error';
                }
            },
            error =>{
                var errorMessage = <any>error;
                console.log(errorMessage);
                if(errorMessage != null){
                    this.status = 'error';
                }
            }
        )
    }

    public noMore = false;
    viewMore(){
        this.page += 1;
        // Para ocultar boton
        if(this.page == this.pages){
            this.noMore = true;
        }

        this.getPublications(this.user,this.page,true);
    }


    refresh(){
        this.getPublications(this.user,this.page);
    }

    showThisImage(id){
        this.showImage = id;
    }
    hideThisImage(){
        this.showImage = null;
    }

    deletePublication(id){
        this._publicationService.deletePublication(this.token,id).subscribe(
            response => {
                this.refresh();
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if(errorMessage != null){
                    this.status = 'error';
                }
            }
        )
    }

}
