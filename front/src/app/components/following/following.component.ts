import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from "@angular/router";
import { User } from "../../models/user";
import { UserService } from "../../services/user.service";
import { FollowService } from "../../services/follow.service";
import { GLOBAL } from "../../services/global";
import { Follow } from "../../models/follow";

@Component({
    selector: 'app-following',
    templateUrl: './following.component.html',
    styleUrls: ['./following.component.scss'],
    providers: [UserService,FollowService]
})
export class FollowingComponent implements OnInit {
    public page;
    public status:string;
    public next_page;
    public prev_page;
    public title:string;
    public url: string;
    public identity;
    public token;
    public actualUserId;
    public follows;
    public total;
    public pages;
    public users: User[];

    public following;


    constructor(
        private _router:Router,
        private _route:ActivatedRoute,
        private _userService:UserService,
        private _followService:FollowService,
    ) {
        this.url = GLOBAL.url;
        this.title = 'Gente que sigue ';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
    }

    ngOnInit() {
        this.actualPage();
    }

    actualPage(){
        this._route.params.subscribe(params=>{
            let page = +params['page'];
            this.page = page;
            let user_id = params['id'];
            this.actualUserId = user_id;


            if(!params['page']){
                page = 1;
            }
            if(!page){
                page = 1;
            }else{
                this.next_page = page+1;
                this.prev_page = page-1;

                if(this.prev_page <= 0){
                    this.prev_page = 1;
                }
            }

            this.getUser(user_id,page);
            // listado de usuarios
        });
    }

    public user: User[];
    getUser(user_id,page){
      this._userService.getUser(user_id).subscribe(
          response => {
            if(response.user){
                this.user = response.user;
                this.getFollow(user_id,page);
            }else{
              this._router.navigate(['/home']);
            }
          },
          error => {
              var errorMessage = <any>error;
              console.log(errorMessage);
              if(errorMessage != null){
                  this.status = 'error';
              }
          }
      )
    }

    getFollow(user_id,page){
        this._followService.getFollowing(this.token,user_id,page).subscribe(
            response => {
                if(!response.follows){
                    this.status = 'error';
                }else {
                    this.total = response.total;
                    this.following = response.follows;
                    this.pages = response.pages;

                    // Mapeo de objecto a array
                    let followingObject = response.users_following;
                    this.follows = followingObject.map(function (val) {
                        return val.followed;
                    });

                    // if(page > this.pages){
                    //     this._router.navigate(['/home']);
                    // }
                }
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if(errorMessage != null){
                    this.status = 'error';
                }
            }
        );
    }


    public followUserOver;
    mouseEnter(user_id){
        this.followUserOver = user_id;
    }

    mouseLeave(user_id){
        this.followUserOver = null;
    }

    followUser(followed){
        var follow = new Follow('',this.identity._id,followed);

        this._followService.addFollow(this.token,follow).subscribe(
            response => {
                if(!response.followStored){
                    this.status = 'error';
                }else{
                    this.status = 'success';
                    this.follows.push(followed);
                }
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if(errorMessage != null){
                    this.status = 'error';
                }
            }
        )
    }

    unfollowUser(followed){
        this._followService.deleteFollow(this.token,followed).subscribe(
            response => {
                // Busca dentro del array de los que sigo y saca indice
                var search = this.follows.indexOf(followed);
                if(search != -1){
                    this.follows.splice(search, 1);
                }
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if(errorMessage != null){
                    this.status = 'error';
                }
            }
        )
    }

}
