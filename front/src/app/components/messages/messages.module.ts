import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule } from "@angular/forms";
import { MomentModule } from "angular2-moment";

// Servicios

import { UserGuard } from "../../services/user.guard";
import { UserService } from "../../services/user.service";


//Rutas

import { MessageRoutingModule } from "./message-routing.module";

//Componentes
import { MainComponent } from './components/main/main.component';
import { AddComponent } from './components/add/add.component';
import { ReceivedComponent } from './components/received/received.component';
import { SendedComponent } from './components/sended/sended.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MessageRoutingModule,
    MomentModule
  ],
  declarations: [
      MainComponent,
      AddComponent,
      ReceivedComponent,
      SendedComponent
  ],
    exports: [
        MainComponent,
        AddComponent,
        ReceivedComponent,
        SendedComponent,
        MessageRoutingModule
    ],
    providers: [UserGuard,UserService]
})
export class MessagesModule { }
