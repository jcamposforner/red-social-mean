import {NgModule} from "@angular/core";
import {Routes,RouterModule} from "@angular/router";


//Componentes
import { MainComponent } from './components/main/main.component';
import { AddComponent } from './components/add/add.component';
import { ReceivedComponent } from './components/received/received.component';
import { SendedComponent } from './components/sended/sended.component';
import { HomeComponent} from "../home/home.component";
import {UserGuard} from "../../services/user.guard";

const messageRoutes: Routes = [
    {
        path: 'mensajes',
        component: MainComponent,
        children: [
            {
                path: '',redirectTo: 'recibidos/1',pathMatch: 'full', canActivate:[UserGuard]
            },
            {
                path: 'enviar',component:AddComponent, canActivate:[UserGuard]
            },
            {
                path: 'recibidos',redirectTo: 'recibidos/1',pathMatch: 'full', canActivate:[UserGuard]
            },
            {
                path: 'recibidos/:page',component:ReceivedComponent, canActivate:[UserGuard]
            },
            {
                path: 'enviados',redirectTo: 'enviados/1',pathMatch: 'full', canActivate:[UserGuard]
            },
            {
                path: 'enviados/:page',component:SendedComponent, canActivate:[UserGuard]
            },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(messageRoutes)
    ],
    exports: [
        RouterModule
    ],
})

export class MessageRoutingModule {}