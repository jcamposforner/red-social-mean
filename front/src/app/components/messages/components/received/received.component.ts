import { Component, OnInit, DoCheck } from '@angular/core';
import {Message} from "../../../../models/message";
import {FollowService} from "../../../../services/follow.service";
import {MessageService} from "../../../../services/message.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../../services/user.service";
import {GLOBAL} from "../../../../services/global";

@Component({
  selector: 'app-received',
  templateUrl: './received.component.html',
  styleUrls: ['./received.component.scss'],
    providers: [FollowService,MessageService]
})
export class ReceivedComponent implements OnInit {


    public title:string;
    public messages: Message[];
    public identity;
    public token;
    public url;
    public status;
    public follows;
    public pages;
    public page;
    public total;
    public next_page;
    public prev_page;

    constructor(
        private _followService: FollowService,
        private _messageService:MessageService,
        private _route:ActivatedRoute,
        private _router:Router,
        private _userService:UserService,
    ) {

        this.title = 'Mensajes recibidos';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.url = GLOBAL.url;
    }

    ngOnInit() {
        this.actualPage();
    }

    actualPage(){
        this._route.params.subscribe(params=>{
            let page = +params['page'];
            this.page = page;
            let user_id = params['id'];


            if(!params['page']){
                page = 1;
            }
            if(!page){
                page = 1;
            }else{
                this.next_page = page+1;
                this.prev_page = page-1;

                if(this.prev_page <= 0){
                    this.prev_page = 1;
                }
            }

            this.getMessages(this.token, this.page);
            // listado de usuarios
        });
    }

    getMessages(token,page){
        this._messageService.getMyMessages(token,page).subscribe(
            response => {
                if(response.messages){
                    this.messages = response.messages;
                    this.total = response.total;
                    console.log(response)
                    this.pages = response.pages;
                }
            },
            error => {
                console.log(<any>error);
            }
        )
    }

}
