import { Component, OnInit } from '@angular/core';
import { Params,ActivatedRoute, Router } from "@angular/router";
import { User } from "../../models/user";
import { UserService } from "../../services/user.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [UserService]
})
export class RegisterComponent implements OnInit {
    public title:string;
    public user: User;
    public status: string;

    constructor(
        private _route:ActivatedRoute,
        private _router:Router,
        private _userService:UserService) {
        this.title = 'Registrate';
        this.user = new User(
            "",
            "",
            "",
            "",
            "",
            "",
            "ROLE_USER",
            ""
        );
    }

    onSubmit(){
        this._userService.register(this.user).subscribe(
            response => {
                if(response.user && response.user._id){
                    this.status = 'success';
                }else{
                    this.status = 'error';
                }
            },
            error => {
                console.log(<any>error)
            }
        );
    }

  ngOnInit() {
  }

}
