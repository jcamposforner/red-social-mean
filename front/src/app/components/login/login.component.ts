import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { User } from "../../models/user";
import { UserService } from "../../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
    providers:[UserService]
})
export class LoginComponent implements OnInit {
  public title:string;
  public user:User;
  public identity;
  public token;
  public status:string;

  constructor(private _route:ActivatedRoute,
              private _router:Router,
              private _userService:UserService) {
              this.title = 'Identificate';
              this.user = new User(
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "ROLE_USER",
                  ""
              );
  }

    onSubmit(){
      this._userService.signup(this.user).subscribe(
        response => {
          this.identity = response.user;
          if(!this.identity || !this.identity._id){
            this.status = 'error';
          }else{

            // Local Storage para acceder a cualquier componente
              localStorage.setItem('identity', JSON.stringify(this.identity));

            // Persistencia (token)
            this.getToken()
          }
        },
        error => {
          var errorMessage = <any>error;
          if(errorMessage != null){
            this.status = 'error';
          }
        }
      );
    }

    getToken(){
        this._userService.signup(this.user,true).subscribe(
            response => {
                this.token = response.token;
                if(this.token.length <= 0){
                    this.status = 'error';
                }else{
                    // Persistencia (token)
                    localStorage.setItem('token', JSON.stringify(this.token));

                    // Conseguir contadores
                    this.getCounters();


                }
            },
            error => {
                var errorMessage = <any>error;
                if(errorMessage != null){
                    this.status = 'error';
                }
            }
        );
    }


    getCounters(){
      this._userService.getCounters().subscribe(
          response =>{
              localStorage.setItem('stats',JSON.stringify(response));
              this.status = 'success';
              this._router.navigate(['/home']);
          },
          error => {
              console.log(<any>error);
          }
      )
    }

  ngOnInit() {
  }

}
