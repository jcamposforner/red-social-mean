import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import { UserService } from "../../services/user.service";
import {GLOBAL} from "../../services/global";
import { PublicationService } from "../../services/publication.service";
import { Publication } from "../../models/publication";
import { UploadService } from "../../services/upload.service";
import { ActivatedRoute, Params, Router } from "@angular/router";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
    providers:[UserService,PublicationService,UploadService]
})
export class SidebarComponent implements OnInit {
  public url: string;
  public token;
  public stats;
  public identity;
  public title:string;
  public status;
  public publication: Publication;




  constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _uploadService: UploadService,
        private _userService:UserService,
        private _publicationService:PublicationService,
      ) {
        this.title = 'Mis datos';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        // this.stats = this._userService.getStats();
        this.stats = this.getCounters(this.identity._id);
        this.url = GLOBAL.url;
        this.publication = new Publication('','','','',this.identity._id);
      }


  ngOnInit() {
  }

  onSubmit(form, $event){
    this._publicationService.addPublication(this.token, this.publication).subscribe(
        response =>{
            if(response.publication){
              // this.publication = response.publication;


              // Subir imagen
                if(this.filesToUpload){
                    this._uploadService.makeFileRequest(`${this.url}upload-image-pub/${response.publication._id}`,[],this.filesToUpload,this.token,'image')
                        .then((result:any) => {
                          this.publication.file = result.image
                          this.status = 'success';
                          form.reset();
                          this._router.navigate(["/publicaciones"]);
                          this.sent.emit({send:true});
                        });
                }else{
                    this.status = 'success';
                    form.reset();
                    this._router.navigate(["/publicaciones"]);
                    this.sent.emit({send:true});
                }

            }else{
                this.status = 'error';
            }
        },
        error => {
          var errorMessage = <any>error;
          console.log(errorMessage);
          if(errorMessage != null){
            this.status = 'error';
          }
        }
    )
  }


  public filesToUpload: Array<File>;

  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }



    // Output
    @Output() sent = new EventEmitter();


    sendPublication(event){
      this.sent.emit({send:'true'});
    }

    getCounters(id){
        this._userService.getCounters(id).subscribe(
            response => {
                this.stats = response;
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
            }
        )

    }
}
