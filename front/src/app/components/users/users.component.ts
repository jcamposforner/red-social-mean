import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from "@angular/router";
import { User } from "../../models/user";
import { UserService } from "../../services/user.service";
import { FollowService } from "../../services/follow.service";
import { GLOBAL } from "../../services/global";
import { Follow } from "../../models/follow";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
    providers: [UserService,FollowService]
})
export class UsersComponent implements OnInit {
  public page;
  public status:string;
  public next_page;
  public prev_page;
  public title:string;
  public url: string;
  public identity;
  public token;

  public follows;
  public total;
  public pages;
  public users: User[];

  constructor(
        private _router:Router,
        private _route:ActivatedRoute,
        private _userService:UserService,
        private _followService:FollowService,
        ) {
          this.url = GLOBAL.url;
          this.title = 'Usuarios';
          this.identity = this._userService.getIdentity();
          this.token = this._userService.getToken();
        }

  ngOnInit() {
    this.actualPage();
  }

  actualPage(){
    this._route.params.subscribe(params=>{
      let page = +params['page'];
      this.page = page;

      if(!params['page']){
        page = 1;
      }
      if(!page){
        page = 1;
      }else{
        this.next_page = page+1;
        this.prev_page = page-1;

        if(this.prev_page <= 0){
          this.prev_page = 1;
        }
      }
      this.getUsers(page);
      // listado de usuarios
    });
  }

  getUsers(page){
    this._userService.getUsers(page).subscribe(
        response => {
          if(!response.users){
            this.status = 'error';
          }else {
            this.total = response.total;
            this.users = response.users;
            this.pages = response.pages;

            // Mapeo de objecto a array
            let followingObject = response.users_following;
            this.follows = followingObject.map(function (val) {
               return val.followed;
            });

            if(page > this.pages){
              this._router.navigate(['/home']);
            }
          }
        },
        error => {
          var errorMessage = <any>error;
          console.log(errorMessage);
          if(errorMessage != null){
            this.status = 'error';
          }
        }
    );
  }


  public followUserOver;
  mouseEnter(user_id){
    this.followUserOver = user_id;
  }

  mouseLeave(user_id){
      this.followUserOver = null;
  }

  followUser(followed){
    var follow = new Follow('',this.identity._id,followed);

    this._followService.addFollow(this.token,follow).subscribe(
        response => {
          if(!response.followStored){
            this.status = 'error';
          }else{
            this.status = 'success';
            this.follows.push(followed);
          }
        },
        error => {
            var errorMessage = <any>error;
            console.log(errorMessage);
            if(errorMessage != null){
                this.status = 'error';
            }
        }
    )
  }

  unfollowUser(followed){
    this._followService.deleteFollow(this.token,followed).subscribe(
        response => {
          // Busca dentro del array de los que sigo y saca indice
          var search = this.follows.indexOf(followed);
          if(search != -1){
            this.follows.splice(search, 1);
          }
        },
        error => {
            var errorMessage = <any>error;
            console.log(errorMessage);
            if(errorMessage != null){
                this.status = 'error';
            }
        }
    )
  }

}
