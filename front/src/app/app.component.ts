import { Component, OnInit, DoCheck } from '@angular/core';
import { Router,ActivatedRoute,Params } from "@angular/router";
import { GLOBAL } from "./services/global";
import { UserService } from "./services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
    providers: [UserService]
})
export class AppComponent implements OnInit, DoCheck{
  public title: string;
  public identity;
  public url:string;
    constructor(
        private _router:Router,
        private _route: ActivatedRoute,
        private _userService:UserService,
        ) {
        this.title = 'Red social';
        this.url = GLOBAL.url;
        }


    ngOnInit(){
      this.identity = this._userService.getIdentity();
    }

    // detecta cambios en los componentes
    ngDoCheck(){
      this.identity = this._userService.getIdentity();
    }

    logout(){
      localStorage.clear();
      this.identity = null;
      this._router.navigate(['/']);
    }

}
