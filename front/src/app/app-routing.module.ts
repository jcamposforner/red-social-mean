import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {RegisterComponent} from "./components/register/register.component";
import { HomeComponent } from "./components/home/home.component";
import { UserEditComponent } from "./components/user-edit/user-edit.component";
import { UsersComponent } from "./components/users/users.component";
import {PublicationsComponent} from "./components/publications/publications.component";
import { ProfileComponent } from "./components/profile/profile.component";
import {FollowingComponent} from "./components/following/following.component";
import {FollowsComponent} from "./components/follows/follows.component";


import { UserGuard } from "./services/user.guard";
import {DemoComponent} from "./components/calendar/calendar.component";

const routes: Routes = [
    {path: 'calendario',component:DemoComponent}
    {path: '',component:LoginComponent},
    {path:'home',component:HomeComponent, canActivate:[UserGuard]},
    {path:'login',component:LoginComponent},
    {path:'registro',component:RegisterComponent},
    {path:'mis-datos',component:UserEditComponent, canActivate:[UserGuard]},
    {path:'gente',component:UsersComponent, canActivate:[UserGuard]},
    {path:'gente/:page',component:UsersComponent, canActivate:[UserGuard]},
    {path:'publicaciones',component:PublicationsComponent, canActivate:[UserGuard]},
    {path:'publicaciones/:page',component:PublicationsComponent, canActivate:[UserGuard]},
    {path:'perfil/:id',component:ProfileComponent, canActivate:[UserGuard]},
    {path: 'siguiendo/:id',component:FollowingComponent, canActivate:[UserGuard]},
    {path: 'siguiendo/:id/:page',component:FollowingComponent, canActivate:[UserGuard]},
    {path: 'seguidores/:id',component:FollowsComponent, canActivate:[UserGuard]},
    {path: 'seguidores/:id/:page',component:FollowsComponent, canActivate:[UserGuard]},
    // 404
    // {path:'**',component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
