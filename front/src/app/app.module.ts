import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { MomentModule } from "angular2-moment";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CalendarModule, DateAdapter } from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



// Modulo mensajes

import { MessagesModule } from "./components/messages/messages.module";

// Serivicos guard

import { UserGuard } from "./services/user.guard";
import { UserService } from "./services/user.service";

// Componentes

import {DemoComponent} from "./components/calendar/calendar.component";
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { PublicationsComponent } from './components/publications/publications.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { FollowingComponent } from './components/following/following.component';
import { FollowsComponent } from './components/follows/follows.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserEditComponent,
    UsersComponent,
    SidebarComponent,
    PublicationsComponent,
    ProfileComponent,
    TimelineComponent,
    FollowingComponent,
    DemoComponent,
    FollowsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MessagesModule,
    MomentModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    CalendarModule.forRoot({
        provide: DateAdapter,
        useFactory: adapterFactory,
    })
  ],
  providers: [UserService,UserGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
